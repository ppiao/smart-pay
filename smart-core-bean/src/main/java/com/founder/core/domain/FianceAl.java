package com.founder.core.domain;

import com.opencsv.bean.CsvBindByPosition;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "t_fiance_al")
public class FianceAl implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    @CsvBindByPosition(position=0)
    private String alorder;//支付宝交易号
    @CsvBindByPosition(position=1)
    private String bzorder;//商户订单号
    @CsvBindByPosition(position=2)
    private String tradetype;//业务类型
    @CsvBindByPosition(position=3)
    private String bztitle;//商品名称
    @CsvBindByPosition(position=4)
    private String createtime;//创建时间
    @CsvBindByPosition(position=5)
    private String tradetime;//完成时间
    @CsvBindByPosition(position=6)
    private String much;//门店编号
    @CsvBindByPosition(position=8)
    private String opera;//操作员
    @CsvBindByPosition(position=9)
    private String deviceid;//终端号
    @CsvBindByPosition(position=10)
    private String tradeuser;//对方账户
    @CsvBindByPosition(position=11)
    private BigDecimal totalmoney;//订单金额（元）
    @CsvBindByPosition(position=12)
    private BigDecimal trademoney;//商家实收（元）
    @CsvBindByPosition(position=13)
    private BigDecimal redpacketmoney;//支付宝红包（元）
    @CsvBindByPosition(position=14)
    private BigDecimal jifenmoney;//集分宝（元）
    @CsvBindByPosition(position=15)
    private BigDecimal alipaytax;//支付宝优惠（元）
    @CsvBindByPosition(position=16)
    private BigDecimal tradetax;//商家优惠（元）
    @CsvBindByPosition(position=17)
    private BigDecimal cancletax;//券核销金额（元）
    @CsvBindByPosition(position=18)
    private String taxname;//券名称
    @CsvBindByPosition(position=19)
    private BigDecimal redpacketmoneytax;//商家红包消费金额（元）
    @CsvBindByPosition(position=20)
    private BigDecimal cardtrademoney;//卡消费金额（元）
    @CsvBindByPosition(position=21)
    private String tforder;//退款批次号/请求号
    @CsvBindByPosition(position=22)
    private BigDecimal servicemoney;//服务费（元）
    @CsvBindByPosition(position=23)
    private BigDecimal fenrun;//分润（元）
    @CsvBindByPosition(position=24)
    private String mark;//备注
    private String date;//对账日期
    private String mchid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMchid() {
        return mchid;
    }

    public void setMchid(String mchid) {
        this.mchid = mchid;
    }

    public String getTradetype() {
        return tradetype;
    }

    public void setTradetype(String tradetype) {
        this.tradetype = tradetype;
    }

    public String getBzorder() {
        return bzorder;
    }

    public void setBzorder(String bzorder) {
        this.bzorder = bzorder;
    }

    public String getAlorder() {
        return alorder;
    }

    public void setAlorder(String alorder) {
        this.alorder = alorder;
    }

    public BigDecimal getTrademoney() {
        return trademoney;
    }

    public void setTrademoney(BigDecimal trademoney) {
        this.trademoney = trademoney;
    }

    public String getBztitle() {
        return bztitle;
    }

    public void setBztitle(String bztitle) {
        this.bztitle = bztitle;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getTradetime() {
        return tradetime;
    }

    public void setTradetime(String tradetime) {
        this.tradetime = tradetime;
    }

    public String getMuch() {
        return much;
    }

    public void setMuch(String much) {
        this.much = much;
    }

    public String getOpera() {
        return opera;
    }

    public void setOpera(String opera) {
        this.opera = opera;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getTradeuser() {
        return tradeuser;
    }

    public void setTradeuser(String tradeuser) {
        this.tradeuser = tradeuser;
    }

    public BigDecimal getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(BigDecimal totalmoney) {
        this.totalmoney = totalmoney;
    }

    public BigDecimal getRedpacketmoney() {
        return redpacketmoney;
    }

    public void setRedpacketmoney(BigDecimal redpacketmoney) {
        this.redpacketmoney = redpacketmoney;
    }

    public BigDecimal getJifenmoney() {
        return jifenmoney;
    }

    public void setJifenmoney(BigDecimal jifenmoney) {
        this.jifenmoney = jifenmoney;
    }

    public BigDecimal getAlipaytax() {
        return alipaytax;
    }

    public void setAlipaytax(BigDecimal alipaytax) {
        this.alipaytax = alipaytax;
    }

    public BigDecimal getTradetax() {
        return tradetax;
    }

    public void setTradetax(BigDecimal tradetax) {
        this.tradetax = tradetax;
    }

    public BigDecimal getCancletax() {
        return cancletax;
    }

    public void setCancletax(BigDecimal cancletax) {
        this.cancletax = cancletax;
    }

    public String getTaxname() {
        return taxname;
    }

    public void setTaxname(String taxname) {
        this.taxname = taxname;
    }

    public BigDecimal getRedpacketmoneytax() {
        return redpacketmoneytax;
    }

    public void setRedpacketmoneytax(BigDecimal redpacketmoneytax) {
        this.redpacketmoneytax = redpacketmoneytax;
    }

    public BigDecimal getCardtrademoney() {
        return cardtrademoney;
    }

    public void setCardtrademoney(BigDecimal cardtrademoney) {
        this.cardtrademoney = cardtrademoney;
    }

    public String getTforder() {
        return tforder;
    }

    public void setTforder(String tforder) {
        this.tforder = tforder;
    }

    public BigDecimal getServicemoney() {
        return servicemoney;
    }

    public void setServicemoney(BigDecimal servicemoney) {
        this.servicemoney = servicemoney;
    }

    public BigDecimal getFenrun() {
        return fenrun;
    }

    public void setFenrun(BigDecimal fenrun) {
        this.fenrun = fenrun;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", mchid=").append(mchid);
        sb.append(", tradetype=").append(tradetype);
        sb.append(", bzorder=").append(bzorder);
        sb.append(", alorder=").append(alorder);
        sb.append(", trademoney=").append(trademoney);
        sb.append(", bztitle=").append(bztitle);
        sb.append(", createtime=").append(createtime);
        sb.append(", tradetime=").append(tradetime);
        sb.append(", much=").append(much);
        sb.append(", opera=").append(opera);
        sb.append(", deviceid=").append(deviceid);
        sb.append(", tradeuser=").append(tradeuser);
        sb.append(", totalmoney=").append(totalmoney);
        sb.append(", redpacketmoney=").append(redpacketmoney);
        sb.append(", jifenmoney=").append(jifenmoney);
        sb.append(", alipaytax=").append(alipaytax);
        sb.append(", tradetax=").append(tradetax);
        sb.append(", cancletax=").append(cancletax);
        sb.append(", taxname=").append(taxname);
        sb.append(", redpacketmoneytax=").append(redpacketmoneytax);
        sb.append(", cardtrademoney=").append(cardtrademoney);
        sb.append(", tforder=").append(tforder);
        sb.append(", servicemoney=").append(servicemoney);
        sb.append(", fenrun=").append(fenrun);
        sb.append(", mark=").append(mark);
        sb.append(", date=").append(date);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        FianceAl other = (FianceAl) that;
        return (this.getMchid() == null ? other.getMchid() == null : this.getMchid().equals(other.getMchid()))
            && (this.getTradetype() == null ? other.getTradetype() == null : this.getTradetype().equals(other.getTradetype()))
            && (this.getBzorder() == null ? other.getBzorder() == null : this.getBzorder().equals(other.getBzorder()))
            && (this.getAlorder() == null ? other.getAlorder() == null : this.getAlorder().equals(other.getAlorder()))
            && (this.getTrademoney() == null ? other.getTrademoney() == null : this.getTrademoney().equals(other.getTrademoney()))
            && (this.getBztitle() == null ? other.getBztitle() == null : this.getBztitle().equals(other.getBztitle()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getTradetime() == null ? other.getTradetime() == null : this.getTradetime().equals(other.getTradetime()))
            && (this.getMuch() == null ? other.getMuch() == null : this.getMuch().equals(other.getMuch()))
            && (this.getOpera() == null ? other.getOpera() == null : this.getOpera().equals(other.getOpera()))
            && (this.getDeviceid() == null ? other.getDeviceid() == null : this.getDeviceid().equals(other.getDeviceid()))
            && (this.getTradeuser() == null ? other.getTradeuser() == null : this.getTradeuser().equals(other.getTradeuser()))
            && (this.getTotalmoney() == null ? other.getTotalmoney() == null : this.getTotalmoney().equals(other.getTotalmoney()))
            && (this.getRedpacketmoney() == null ? other.getRedpacketmoney() == null : this.getRedpacketmoney().equals(other.getRedpacketmoney()))
            && (this.getJifenmoney() == null ? other.getJifenmoney() == null : this.getJifenmoney().equals(other.getJifenmoney()))
            && (this.getAlipaytax() == null ? other.getAlipaytax() == null : this.getAlipaytax().equals(other.getAlipaytax()))
            && (this.getTradetax() == null ? other.getTradetax() == null : this.getTradetax().equals(other.getTradetax()))
            && (this.getCancletax() == null ? other.getCancletax() == null : this.getCancletax().equals(other.getCancletax()))
            && (this.getTaxname() == null ? other.getTaxname() == null : this.getTaxname().equals(other.getTaxname()))
            && (this.getRedpacketmoneytax() == null ? other.getRedpacketmoneytax() == null : this.getRedpacketmoneytax().equals(other.getRedpacketmoneytax()))
            && (this.getCardtrademoney() == null ? other.getCardtrademoney() == null : this.getCardtrademoney().equals(other.getCardtrademoney()))
            && (this.getTforder() == null ? other.getTforder() == null : this.getTforder().equals(other.getTforder()))
            && (this.getServicemoney() == null ? other.getServicemoney() == null : this.getServicemoney().equals(other.getServicemoney()))
            && (this.getFenrun() == null ? other.getFenrun() == null : this.getFenrun().equals(other.getFenrun()))
            && (this.getMark() == null ? other.getMark() == null : this.getMark().equals(other.getMark()))
            && (this.getDate() == null ? other.getDate() == null : this.getDate().equals(other.getDate()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getMchid() == null) ? 0 : getMchid().hashCode());
        result = prime * result + ((getTradetype() == null) ? 0 : getTradetype().hashCode());
        result = prime * result + ((getBzorder() == null) ? 0 : getBzorder().hashCode());
        result = prime * result + ((getAlorder() == null) ? 0 : getAlorder().hashCode());
        result = prime * result + ((getTrademoney() == null) ? 0 : getTrademoney().hashCode());
        result = prime * result + ((getBztitle() == null) ? 0 : getBztitle().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getTradetime() == null) ? 0 : getTradetime().hashCode());
        result = prime * result + ((getMuch() == null) ? 0 : getMuch().hashCode());
        result = prime * result + ((getOpera() == null) ? 0 : getOpera().hashCode());
        result = prime * result + ((getDeviceid() == null) ? 0 : getDeviceid().hashCode());
        result = prime * result + ((getTradeuser() == null) ? 0 : getTradeuser().hashCode());
        result = prime * result + ((getTotalmoney() == null) ? 0 : getTotalmoney().hashCode());
        result = prime * result + ((getRedpacketmoney() == null) ? 0 : getRedpacketmoney().hashCode());
        result = prime * result + ((getJifenmoney() == null) ? 0 : getJifenmoney().hashCode());
        result = prime * result + ((getAlipaytax() == null) ? 0 : getAlipaytax().hashCode());
        result = prime * result + ((getTradetax() == null) ? 0 : getTradetax().hashCode());
        result = prime * result + ((getCancletax() == null) ? 0 : getCancletax().hashCode());
        result = prime * result + ((getTaxname() == null) ? 0 : getTaxname().hashCode());
        result = prime * result + ((getRedpacketmoneytax() == null) ? 0 : getRedpacketmoneytax().hashCode());
        result = prime * result + ((getCardtrademoney() == null) ? 0 : getCardtrademoney().hashCode());
        result = prime * result + ((getTforder() == null) ? 0 : getTforder().hashCode());
        result = prime * result + ((getServicemoney() == null) ? 0 : getServicemoney().hashCode());
        result = prime * result + ((getFenrun() == null) ? 0 : getFenrun().hashCode());
        result = prime * result + ((getMark() == null) ? 0 : getMark().hashCode());
        result = prime * result + ((getDate() == null) ? 0 : getDate().hashCode());
        return result;
    }
}