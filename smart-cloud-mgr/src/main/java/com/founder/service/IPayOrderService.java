package com.founder.service;

import com.founder.core.domain.PayOrder;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IPayOrderService {

    PayOrder selectPayOrder(String payOrderId);

    Page<PayOrder> selectPayOrderList(int offset, int limit, PayOrder payOrder);

    @Deprecated
    List<PayOrder> getPayOrderList(int offset, int limit, PayOrder payOrder);

    @Deprecated
    Integer count(PayOrder payOrder);

}
